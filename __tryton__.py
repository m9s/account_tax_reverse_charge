# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Tax Reverse-Charge',
    'name_de_DE': 'Buchhaltung Steuern Abzugsverfahren (Reverse-Charge)',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Reverse-Charge for Taxes
    - Provides the handling of the Reverse-Charge concept for taxes.
''',
    'description_de_DE': '''Reverse-Charge für Steuern
    - Stellt das Konzept des Abzugsverfahrens für Steuern (Reverse-Charge)
      zur Verfügung.
''',
    'depends': [
        'account',
    ],
    'xml': [
        'tax.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
