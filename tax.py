#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class TaxTemplate(ModelSQL, ModelView):
    _name = 'account.tax.template'

    reverse_charge = fields.Boolean('Reverse Charge', readonly=True)

    def _get_tax_value(self, template, tax=None):
        '''
        Set values for tax creation.

        :param template: the BrowseRecord of the template
        :param tax: the BrowseRecord of the tax to update
        :return: a dictionary with account fields as key and values as value
        '''
        res = super(TaxTemplate, self)._get_tax_value(template, tax=tax)
        if not tax or tax.reverse_charge != template.reverse_charge:
                res['reverse_charge'] = template.reverse_charge
        return res

TaxTemplate()


class Tax(ModelSQL, ModelView):
    _name = 'account.tax'

    reverse_charge = fields.Boolean('Reverse Charge')

    def __init__(self):
        super(Tax, self).__init__()
        self._constraints += [
            ('check_reverse_charge', 'all_reverse_charge'),
            ]
        self._error_messages.update({
            'all_reverse_charge': 'All child taxes must be either '
            'set to reverse charge or not!',
            })

    def check_reverse_charge(self, ids):
        if isinstance(ids, (int, long)):
            ids = [ids]
        res = True
        for tax in self.browse(ids):
            if tax.parent:
                if tax.parent.reverse_charge != tax.reverse_charge:
                    res = False
                    break
                if tax.parent.parent:
                    if not self.check_reverse_charge(tax.parent.id):
                        res = False
                        break
            if tax.childs:
                for child in tax.childs:
                    if not self.check_reverse_charge(child.id):
                        res = False
                        break
        return res

Tax()
